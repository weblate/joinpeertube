---
id: crowdfunding-1
title: 'Newsletter du crowdfunding de PeerTube n°1'
date: 'July 23, 2018'
---

<p>Bonjour à toutes et à tous !</p>
<p>Tout
  d'abord, un grand merci pour avoir contribué à notre campagne. ❤️</p>
<p>Pendant le crowdfunding, nous avons continué d'avancer sur le système
  d'internationalisation. Et nous avons le plaisir d'annoncer qu'il est enfin terminé : il sera disponible lors de la
  prochaine version beta de PeerTube (beta 10). À l'heure où nous écrivons ces lignes, l'interface est disponible en
  anglais, français, basque, catalan, tchèque et en esperanto (un énorme merci aux traducteurs). Si vous aussi vous
  voulez aider à la traduction de PeerTube, n'hésitez pas à jeter un coup d'oeil à la documentation !</p>
<p>En ce qui concerne les flux RSS, ils ont été implémentés par Rigelk et sont d'ores et
  déjà utilisables avec la beta 9. Vous pouvez, par exemple, avoir le flux des dernières vidéos locales ajoutées sur une
  instance.</p>
<p>Le support des sous-titres avance bien et nous devrions avoir une première version de
  prête sous peu. Une fois ce chantier terminé, nous passerons à l'implémentation de la recherche avancée.</p>
<p>Nous vous rappelons que vous pouvez suivre l'avancée des travaux directement en
  consultant le dépôt Git, et même participer aux discussions/signalement de bugs/propositions d'améliorations dans
  l'onglet "Issues".</p>
<p>Si vous avez des questions <a target="_blank" rel="noopener noreferrer"
    href="https://framacolibri.org/c/qualite/peertube">un forum</a> est à votre disposition. Vous pouvez également nous
  contacter directement sur <a target="_blank" rel="noopener noreferrer"
    href="https://contact.framasoft.org">https://contact.framasoft.org.</a></p>
<p><span>Librement,</span><br> Framasoft </p>
