---
id: crowdfunding-1
title: 'PeerTube crowdfunding newsletter #1'
date: 'July 23, 2018'
---

Hello everyone!

First of all, thank you again for contributing to PeerTube! ❤️

During the crowdfunding campaign, we continued to work on the localization system. And we are happy to announce it's finally completed: it will be available in the next beta (beta 10) of PeerTube. As of this writing, the web interface is already available in english, french, basque, catalan, czech and esperanto (huge thank you to all of the translators). If you too want to help translating PeerTube, do not hesitate to check out the documentation!

Regarding the RSS feeds feature, it was already implemented by Rigelk and you can already use it in the beta 9. You can, for example, get the feed of the last local videos uploaded in a particular instance.

Subtitles support is well under way, and we should have a first version available soon. When this work is finished, we will develop the advanced search.

We remind you that you can track the progress of the work directly on the git repository, and be part of the discussions/bug reports/feature requests in the "Issues" tab.

Moreover, you can ask questions on [the PeerTube forum](https://framacolibri.org/c/qualite/peertube). You can also contact us directly on https://contact.framasoft.org.

Cheers,
Framasoft
