---
id: release-3.2
title: La 3.2 de PeerTube est sortie !
date: May 27, 2021
---

Bonjour à toutes et à tous,

Il y a 2 mois de cela était publiée la version 3.1 ! Et depuis, nous avons apporté de nombreuses améliorations à votre outil préféré... Voici donc quelques précisions sur ce que vous apporte cette version 3.2.

#### Des améliorations pour mieux distinguer les chaînes des comptes

Dans le cadre de notre travail avec la designer Marie-Cécile Godwin, nous avions constaté qu'il n'était pas aisé de distinguer les comptes des chaînes. Afin de vous permettre de mieux vous y retrouver, nous nous sommes lancés dans une refonte de l'aspect de ces deux éléments. Ainsi l'affichage des avatars des chaînes (au format carré) se distingue désormais de celui des comptes (en forme de cercle).

![img](/img/news/release-3.2/fr/chaine-nvdesign-FR.png)

Comme vous pouvez le constater sur cette capture d'écran, il est maintenant plus évident d'identifier qu'on est sur la page d'une chaîne (avatar carré et indication "chaîne vidéo" au-dessus du titre de la chaîne). En plus, grâce au bloc situé sur la droite, vous pouvez identifier quel compte gère cette chaîne. D'ailleurs, si vous avez la curiosité de cliquer sur le bouton "voir le compte", vous arriverez sur une page qui liste les différentes chaînes du compte. Le design de cette page a aussi été revu afin qu'il soit plus aisé de distinguer les chaînes les unes des autres.

Nous avons aussi ajouté de **nouvelles fonctionnalités pour rendre plus attractives les chaînes**. Vous pouvez ainsi y ajouter une bannière d'illustration et un bouton "soutenir" par lequel vous indiquez comment les internautes peuvent participer au financement de vos créations vidéos.

Enfin, la chaîne (plutôt que le compte) s'affiche désormais en priorité sur les vignettes des vidéos. Nous avons d'ailleurs augmenté d'un tiers la taille de ces vignettes de vidéos : il y a ainsi moins de vidéos affichées sur une seule ligne, ce qui est plus bien agréable.

Notre espoir, c'est qu'avec ces améliorations vous serez plus nombreu⋅ses à créer des chaînes pour y publier vos vidéos.

#### De nouvelles fonctionnalités pour améliorer votre utilisation de PeerTube

Cette version 3.2, c'est aussi tout un tas de petites améliorations et nouvelles fonctionnalités qui vont rendre plus agréable votre utilisation de PeerTube. Chacune d'entre elles ne va pas révolutionner vos pratiques, mais nous sommes quand même très satisfaits de les avoir implémentées.

  * Tout d'abord, PeerTube permet désormais la reprise automatique de la lecture d'une vidéo pour les utilisateur⋅ices non connecté⋅es. Ainsi, si vous êtes interrompu lors de votre visionnage, vous n'aurez pas besoin de rechercher où vous en étiez pour continuer à visionner une vidéo.

  * PeerTube autorise aussi la reprise du téléchargement d'une vidéo lorsque celui-ci a été interrompu du fait d'une connexion internet défaillante. C'était assez rageant de devoir recommencer au début un téléchargement (surtout si la vidéo était importante) en cas de coupure. D'ailleurs, cela faisait un moment que vous nous le faisiez remarquer. Et bien, c'est désormais de l'histoire ancienne !

  * Nous avons modifié le type de téléchargement proposé par défaut. Auparavant, lorsque vous cliquiez sur le bouton "Télécharger", c'est le téléchargement par torrent qui était sélectionné. Désormais, c'est le téléchargement direct. Mais vous pouvez bien évidemment toujours modifier ce choix.

  * Lorsqu'une personne a publié de nombreuses vidéos sur son compte, il est parfois compliqué de s'y retrouver. Nous avons donc ajouté la possibilité de trier ses vidéos en fonction de plusieurs critères : date de publication, les plus vues, les plus appréciées, les plus longues, etc. Et vous pouvez aussi n'afficher que vos diffusions en direct.

  * Afin de faciliter l'administration d'une instance, nous avons activé un système de notifications qui indique aux administrateur⋅ices la disponibilité d'une nouvelle version de PeerTube et des plugins installés sur leur instance. Nous espérons ainsi que davantage d'instances seront mises à jour.

#### Des améliorations sur le menu contextuel du lecteur vidéo

Aviez-vous remarqué qu'en faisant un clic droit sur une vidéo en cours de lecture, vous pouviez afficher un menu contextuel ? Cette fonctionnalité existe depuis un moment, mais elle n'était pas mise en avant. Et comme nous venons d'améliorer ce menu, c'est l'occasion de faire découvrir cette fonctionnalité à celleux qui ne l'avaient pas encore identifiée !

![img](/img/news/release-3.2/fr/menu-contextuel.png)

De petites icônes facilitent la compréhension des actions possibles. et nous avons ajouté la catégorie "stats for nerds" qui affiche, comme son nom l'indique, des informations techniques que seul⋅es les geeks les plus chevronné⋅es comprendront ;)

Beaucoup d'autres améliorations ont été apportées dans cette nouvelle version. Vous  pouvez en voir la liste complète (en anglais) sur https://github.com/Chocobozzz/PeerTube/blob/develop/CHANGELOG.md.

Merci à toustes les contributeur⋅ices de PeerTube !
Framasoft
